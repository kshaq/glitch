﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStartingVolume : MonoBehaviour {

    private MusicManager musicManager;

	// Use this for initialization
	void Start () {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        if (!musicManager)
            Debug.LogWarning("MusicManager not found, can't set volume");
        else
            musicManager.SetVolume(PlayerPrefsManager.GetMasterVolume());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
