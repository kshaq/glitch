﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    [Range (0,3)]
    public float autoLoadNextLevelAfter;

    void Start() {
        if (autoLoadNextLevelAfter > 0)
            Invoke("LoadNextLevel", autoLoadNextLevelAfter);
        else if (autoLoadNextLevelAfter <= 0) {
            Debug.Log("Level autoload disabled, use a positive number in seconds");
        }
    }


    public void LoadNextLevel()
    {
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


    public void LoadLevel(string name){
        SceneManager.LoadScene(name);
	}

	public void QuitRequest(){
		Application.Quit ();
	}

}
