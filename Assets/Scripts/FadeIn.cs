﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeIn : MonoBehaviour {

    CanvasGroup startScreeen;
    public float fadeStepTime;

	// Use this for initialization
	void Start () {
        startScreeen = GetComponent<CanvasGroup>();
        startScreeen.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Fade(fadeStepTime, startScreeen));

    }



    IEnumerator Fade(float fadeInStep, CanvasGroup canvasToFade) {

        for (float f = 0; f < 1; f += fadeInStep)
        {
            canvasToFade.alpha += f; 
            yield return null;
        }

    }
}
