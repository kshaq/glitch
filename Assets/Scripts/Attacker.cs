﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {

    [Range(-1f,2f)]
    public float walkSpeed;
    private GameObject currentTarget;
    Animator anim;

	// Use this for initialization
	void Start () {
       Rigidbody2D myRigidbody = gameObject.AddComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        myRigidbody.isKinematic = true;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        if (!currentTarget) {
            anim.SetBool("isAttacking", false);
        }
	}

    public void Move() {
        transform.Translate(Vector3.left * walkSpeed * Time.deltaTime);
    }
    public void SetMovementSpeed(float speed) {
        walkSpeed = speed;
    }

    public void StrikeCurrentTarget(int damage) {
        if (!currentTarget) {
            return;
        } else
        {
            currentTarget.GetComponent<Health>().takeDamage(damage);
        }
      
    }

    public void Attack(GameObject obj) {
        currentTarget = obj;
    }

    
}
