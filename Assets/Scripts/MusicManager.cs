﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;


    void Awake() {
        DontDestroyOnLoad(gameObject);
        audioSource = gameObject.GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
            PlayClip(scene.buildIndex);
    }


    public void SetVolume(float volume) {

        audioSource.volume = volume;
    }
    

    void PlayClip(int clipnumber) {
        AudioClip thisLevelMusic = levelMusicChangeArray[clipnumber];
        audioSource = gameObject.GetComponent<AudioSource>();

        if (thisLevelMusic) {
            audioSource.clip = thisLevelMusic;
            if(clipnumber!=0)
                audioSource.loop = true;
            audioSource.spatialBlend = 0f;
            audioSource.Play();
        }
    }
}
