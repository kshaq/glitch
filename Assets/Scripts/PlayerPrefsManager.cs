﻿using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {

    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    const string LEVEL_KEY = "level_unlocked_";

    const float DEFAULT_VOLUME = 0.5f;
    const float DEFAULT_DIFFICULTY = 2;


    public static void SetMasterVolume(float volume) {

        if (volume < 0 && volume > 1)
        {
            Debug.LogError(MASTER_VOLUME_KEY + " is out of range");
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, DEFAULT_VOLUME);
        }
        PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
    }

    public static float GetMasterVolume() {
        if (PlayerPrefs.GetFloat(MASTER_VOLUME_KEY) >= 0 && PlayerPrefs.GetFloat(MASTER_VOLUME_KEY) <= 1)
            return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
        else
            return DEFAULT_VOLUME;
    }

    public static void SetDifficulty(float difficulty) {
        if (difficulty < 1 && difficulty > 3) {
            Debug.LogError(DIFFICULTY_KEY + " is out of range");
            PlayerPrefs.SetFloat(DIFFICULTY_KEY, DEFAULT_DIFFICULTY);
        }
        
        PlayerPrefs.SetFloat(DIFFICULTY_KEY, difficulty);
    }

    public static float GetDifficulty() {
        if (PlayerPrefs.GetFloat(DIFFICULTY_KEY) >= 1 && PlayerPrefs.GetFloat(DIFFICULTY_KEY) <= 3)
            return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
        else return DEFAULT_DIFFICULTY;
    }

    public static void SetDefaultValues() {
        PlayerPrefs.SetFloat(DIFFICULTY_KEY, DEFAULT_DIFFICULTY);
        PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, DEFAULT_VOLUME);
    }
}
