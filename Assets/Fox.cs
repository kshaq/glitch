﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attacker))]
public class Fox : MonoBehaviour {

    private Attacker attacker;
    private Animator anim;

    // Use this for initialization
    void Awake()
    {
        attacker = gameObject.GetComponent<Attacker>();
        anim = gameObject.GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collided with" + collision.gameObject.name);
        if (collision.gameObject.GetComponent<Defender>())
        {
            if (collision.GetComponent<Stone>()) {
                anim.SetTrigger("jump");
            }
            else
            {
                gameObject.GetComponent<Animator>().SetBool("isAttacking", true);
                attacker.Attack(collision.gameObject);
            }
        }
        else {
            return;
        }
        
    }




}
