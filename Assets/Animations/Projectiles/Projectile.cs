﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 1;

    public int damage = 5 ;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Attacker>()) {
            other.GetComponent<Health>().takeDamage(damage);
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update () {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
	}


}
