﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lizard : MonoBehaviour {
    private Attacker attacker;

    // Use this for initialization
    void Awake()
    {
        attacker = gameObject.GetComponent<Attacker>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collided with" + collision.gameObject.name);
        if (collision.gameObject.GetComponent<Defender>())
        {
            attacker.Attack(collision.gameObject);
            gameObject.GetComponent<Animator>().SetBool("isAttacking", true);
        }
    }


}
